package com.sda.company.controller;

import com.sda.company.components.CustomFakerCompany;
import com.sda.company.exception.CompanyNotFoundException;
import com.sda.company.model.Company;
import com.sda.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//RequestMapping annotation is used to map web requests onto specific handler classes and/or handler methods.
@RestController
@RequestMapping("/api/v1/companies")
@ControllerAdvice
public class CompanyController {

//    @Autowired
//    private CompanyService companyService;
//
//    @Autowired
//    private CustomFakerCompany customFakerCompany;

    private final CompanyService companyService;
    private final CustomFakerCompany customFakerCompany;

    @Autowired
    public CompanyController(CompanyService companyService, CustomFakerCompany customFakerCompany) {
        this.companyService = companyService;
        this.customFakerCompany = customFakerCompany;
    }

//the @RequestBody annotation allows us to retrieve the request's body and automatically convert it to Java Object.
//the @RequestBody annotation binds the HTTPRequest body to the domain object.

    @PostMapping("/create")
    public ResponseEntity<Company> create(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.create(company));
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Company>> getAll() {
        return ResponseEntity.ok(companyService.getAll());
    }

    @GetMapping("/getAllPaginated")
    public ResponseEntity<List<Company>> getAllPaginated(
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "50") Integer pageSize,
            @RequestParam(defaultValue = "name") String sortBy) {
        return ResponseEntity.ok(companyService.getAllPaginated(pageNumber, pageSize, sortBy));
    }

    @GetMapping("/findByName")
    public ResponseEntity<Company> findByName(@RequestParam String name) {
        return ResponseEntity.ok(companyService.findByName(name)
                .orElseThrow(() -> new CompanyNotFoundException("Company with name <" + name + "> not found")));
    }

    @GetMapping("/populate")
    public ResponseEntity<String> populate() {
        return ResponseEntity.ok(companyService.populate(customFakerCompany.createDummyCompanyList()));
    }
}
