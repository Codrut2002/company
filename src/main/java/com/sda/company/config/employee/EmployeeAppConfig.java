package com.sda.company.config.employee;

import com.sda.company.components.CustomFakerEmployee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmployeeAppConfig {

    @Bean
    public CustomFakerEmployee customFakerEmployee() {
        return new CustomFakerEmployee();
    }
}
