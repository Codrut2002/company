package com.sda.company.service;

import com.sda.company.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Employee create(Employee employee);

    String populate(List<Employee> employeeList);

    List<Employee> getAll();

    List<Employee> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy);

    Optional<Employee> findByName(String name);
}
