package com.sda.company.service;

import com.sda.company.model.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyService {

    Company create(Company company);

    String populate(List<Company> companiesList);

    List<Company> getAll();

    List<Company> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy);

    Optional<Company> findByName(String name);
}
