package com.sda.company.service.impl;

import com.sda.company.model.Company;
import com.sda.company.repository.CompanyRepository;
import com.sda.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    private final CompanyRepository companyRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    public Company create(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public String populate(List<Company> companiesList) {
        List<Company> result = (List<Company>) companyRepository.saveAll(companiesList);
        if (result.isEmpty()) {
            return "There is a problem";
        }else {
            return "The list has been created";
        }
    }

    @Override
    public List<Company> getAll() {
        return (List<Company>) companyRepository.findAll();
    }

    @Override
    public List<Company> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy) {
//        return (List<Company>) companyRepository.findAll();
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
        Page<Company> companyPage = companyRepository.findAll(pageable);
        return companyPage.getContent();
    }

    @Override
    public Optional<Company> findByName(String name) {
        return companyRepository.findByName(name);
    }


}
